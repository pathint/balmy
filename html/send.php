﻿<?php
error_reporting(0);
include('./MailChimp.php');
use \DrewM\MailChimp\MailChimp;
$MailChimp = new MailChimp('07a8b614b006fe5d956329985aeb1212-us20');

$FNAME_LNAME = explode(" ", $_POST["FNAME_LNAME"], 2);
$EMAIL = strip_tags(trim($_POST["email_address"]));
$PHONE = strip_tags(trim($_POST["PHONE"]));
$FNAME = strip_tags(trim($FNAME_LNAME[0]));
$LNAME = strip_tags(trim($FNAME_LNAME[1]));

//$result = $MailChimp->get('lists');
//print_r($result);

$list_id = '9a48cf6488';
$result = $MailChimp->post("lists/$list_id/members", [
    'email_address' => $EMAIL,
    'merge_fields'  => [
        'FNAME' =>$FNAME,
        'LNAME' =>$LNAME,
        'PHONE' =>$PHONE
    ],
    'status'    => 'subscribed',
]);

print_r($result);
