/*
 Author: Genio - Yüce Özkan
 Author URL: http://www.genio.com.tr/
 */
$(document).ready(function() {

    var isWebkit = 'WebkitAppearance' in document.documentElement.style;
    var html = $(document).find("html");
    var body = $(document).find("body");
    if(isWebkit){html.addClass("webkit");}
    html.addClass("dom-ready");

    Modernizr.addTest('pointerevents',function(){
        return 'pointerEvents' in document.documentElement.style;
    });

    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if(isFirefox)
    {
        html.addClass("firefox");
    }
    if(isIe())
    {
        html.addClass("ie");
    }

    /*input mask & valid*/
    $(document).on("keypress paste",".numeric",function (e) {
        var key = e.keyCode || e.which;
        var keyArray = [37,38,39,40,8,46];
        if(jQuery.inArray(key,keyArray) > 0) {
            return true;
        }
        else
        {
            var code = e.charCode || e.keyCode;
            if (String.fromCharCode(code).match(/[^0-9]/g)) return false;
        }
    });
    $(document).on("keypress paste",".alphabetical",function (e) {
        var key = e.keyCode || e.which;
        var keyArray = [37,38,39,40,8,46];
        if(jQuery.inArray(key,keyArray) > 0) {
            return true;
        }
        else
        {
            var code = e.charCode || e.keyCode;
            if (String.fromCharCode(code).match(/[^a-zA-ZığüşöçİĞÜŞÖÇ ]/g)) return false;
        }
    });
    $(document).on("keypress paste",".alphanumeric",function (e) {
        var key = e.keyCode || e.which;
        var keyArray = [37,38,39,40,8,46];
        if(jQuery.inArray(key,keyArray) > 0) {
            return true;
        }
        else
        {
            var code = e.charCode || e.keyCode;
            if (String.fromCharCode(code).match(/[^a-zA-Z0-9]/g)) return false;
        }
    });
    $(document).on("keypress paste",".email",function (e) {
        var key = e.keyCode || e.which;
        var keyArray = [37,38,39,40,8,46];
        if(jQuery.inArray(key,keyArray) > 0) {
            return true;
        }
        else
        {
            var code = e.charCode || e.keyCode;
            if (String.fromCharCode(code).match(/[^a-zA-Z0-9-_@]/g)) return false;
        }

    });

    $(".gallery-list li a").attr("rel","gallery").fancybox({
        padding:0,
        margin: [30,60,30,60]
    });



    function initBaloncuk(){
        var baloncukSayisi = 15;
        var baloncukMaxWidth = 70;
        var baloncukMinWidth = 40;
        var baloncukRatio = 1.060606060606061;
        function randomIntFromInterval(min,max){
            return Math.floor(Math.random()*(max-min+1)+min);
        }
        for(var i=0;i<baloncukSayisi;i++){
            var width = randomIntFromInterval(baloncukMinWidth,baloncukMaxWidth);
            var height = Math.ceil(width*baloncukRatio);
            var top = randomIntFromInterval(0,90);
            var left = randomIntFromInterval(0,90);
            var time = randomIntFromInterval(2,6);
            var baloncuk = '<div class="baloncuk" style="width:'+width+'px;height:'+height+'px;top:'+top+'%;left:'+left+'%;animation-duration:'+time+'s"></div>';
            $(document).find("body").append(baloncuk);
        }
        
        /*$(window).load(function(){
            $(document).find(".baloncuk").each(function(){
                $(this).addClass("init");
                $(this).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(e) {
                    $(this).removeClass("init");
                });
            }); 
        });*/

    }
    initBaloncuk();
    
    /**/
    var fullpage = $(document).find(".full-page").fullpage({
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        menu: '',
        lockAnchors: false,
        anchors:[],
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: [],
        normalScrollElements: '',
        animateAnchor: false,
        recordHistory: false,
        sectionsColor : [],
        paddingTop: '0',
        paddingBottom: '0',
        fixedElements: '#sidebar',
        responsiveWidth: 900,
        sectionSelector: '.full-page-section',
        onLeave: function(origin, destination, direction){
            console.log(origin,destination,direction);
        },
        afterLoad: function(origin, destination, direction){
            console.log(origin,destination,direction);
            if(destination == 1)
            {
            }
        },
        afterRender: function(origin, destination, direction){

        },
        afterResize: function(width, height){
        },
        afterResponsive: function(isResponsive){},
        afterSlideLoad: function(section, origin, destination, direction){},
        onSlideLeave: function(section, origin, destination, direction){}
    });

    $(".open-modal").fancybox({
        type: "inline",
        padding:0,
        margin: 20,
        beforeLoad:function(){
            $.fn.fullpage.setAllowScrolling(false);
            $.fn.fullpage.setKeyboardScrolling(false);
        },
        beforeClose:function(){
            $.fn.fullpage.setAllowScrolling(true);
            $.fn.fullpage.setKeyboardScrolling(true);
        }
    });
    
    /**/
    $(window).resize(function(){
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        mobileDetect();

        /*if(windowWidth < 901){
            $(document).find(".section").css("height",windowHeight);
            $(document).find(".section.section-double-height").css("height",(windowHeight*2)-100);
        } else {
            $(document).find(".section").css("height","");
        }

        if(windowWidth < 768){
            $(document).find(".full-double-height").css("height",(windowHeight*2)-100);
        } else {
            $(document).find(".full-double-height").css("height",windowHeight);
        }*/

    }).trigger("resize");

    $(window).resize(function(){
    });

    $(window).scroll(function(){

    }).trigger("scroll");


    var wrapper = $(document);
    init(wrapper);

    $(window).load(function(e){

    });


    function mobileDetect(){
        var html = $(document).find("html");
        var md = new MobileDetect(window.navigator.userAgent);
        html.removeClass("mobile phone tablet");
        if(md.mobile())
        {
            html.addClass("mobile");
        }
        if(md.phone())
        {
            html.addClass("phone");
        }
        if(md.tablet())
        {
            html.addClass("tablet");
        }

        if(html.hasClass("mobile")){
        }
        else
        {
        }
    }


    $(window).scroll(function(){


    }).trigger("scroll");

    $(document).find("input").on("keydown", function(e) {
        if (e.which == 9) {
            e.preventDefault();
            var tab = parseInt($(this).attr("tabindex")) + parseInt(1);
            var form = $(this).parents("form");
            var next = form.find("input[tabindex=" + tab + "]");
            next.focus();
            e.stopPropagation();
        }
    });
    

    $(document).on("click",".nav-trigger",function(e){
        e.preventDefault();
        if(html.hasClass("menu-open"))
        {
            html.removeClass("menu-open");
            e.stopPropagation();
        }
        else {
            html.addClass("menu-open");
            html.removeClass("nav-detail-open");
            $(document).find(".nav-overlay").each(function(){
                $(this).hide();
            });
            e.stopPropagation();
        }

    });
    $(document).on("click",".close-nav",function(e){
        e.preventDefault();
        if(html.hasClass("menu-open"))
        {
            html.removeClass("menu-open");
            e.stopPropagation();
        }
    });
    $(document).on("click",".nav-mobile-overlay",function(e){
        e.preventDefault();
        html.removeClass("menu-open");
    });


    $(document).on("submit",".keepInTouch",function(e){
        e.preventDefault();
        var form = $(this);
        var error = 0;
        form.find("input").each(function(){
            if($(this).val().length == 0)
            {
                error++;
            }
        });
        if(error == 0)
        {
            $.ajax({
                method: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                beforeSend: function() {
                },
                success: function(data){
                    window.location.href = "./tesekkurler.html";
                    //form.find(".form-content").slideUp(400);
                    //form.find(".form-response").slideDown(400);
                }
            });
        }
        else {
            alert("Lütfen tüm alanları doldurunuz!");
        }
    });


});//ready
